# Fine dust forecasting app

<details>
  <summary>the first screen of the app</summary>
<img src="https://gitlab.com/flutter_toys/fine-dust-forecasting-app/uploads/78a2a22d44c95a2dc22b72ee44f41c41/%E1%84%89%E1%85%B3%E1%84%8F%E1%85%B3%E1%84%85%E1%85%B5%E1%86%AB%E1%84%89%E1%85%A3%E1%86%BA_2023-03-06_%E1%84%8B%E1%85%A9%E1%84%8C%E1%85%A5%E1%86%AB_12.22.50.png" height ="50%" width ="50%"></img>
</details>

It is an application with the following characteristics.

> **CustomScrollView** <br>
> **Drawer**<br>
> **Dio**<br>
> **HIVE** <br>
> **Supports offline environments**<br>
> **Government API**<br>

## Getting Started

1. currently using the package below, so please refer to it.

```
  hive: ^2.0.6
  hive_flutter: ^1.1.0
  dio: ^4.0.4
```
※ In this project, **NoSQL** was used to reduce schema generation time because it imports and parses various forms of data. In particular, in the case of **HIVE**, Flutter provides various support and is widely used, so hired it.

2. About **API**

From [this site](https://www.data.go.kr/index.do), you can also obtain API keys and check the information through documents.<br>
Especially, using the API described [here](https://www.data.go.kr/tcs/dss/selectApiDataDetailView.do?publicDataPk=15073855
). 

Plus, this following URL is the only **endpoint** I'm using, so be careful not to use anything else. 

http://apis.data.go.kr/B552584/ArpltnStatsSvc/getCtprvnMesureLIst

## APP Description

<details>
  <summary>System Snapshot</summary>
<img src="https://gitlab.com/flutter_toys/fine-dust-forecasting-app/uploads/78a2a22d44c95a2dc22b72ee44f41c41/%E1%84%89%E1%85%B3%E1%84%8F%E1%85%B3%E1%84%85%E1%85%B5%E1%86%AB%E1%84%89%E1%85%A3%E1%86%BA_2023-03-06_%E1%84%8B%E1%85%A9%E1%84%8C%E1%85%A5%E1%86%AB_12.22.50.png" height ="50%" width ="50%"></img>
<img src="https://gitlab.com/flutter_toys/fine-dust-forecasting-app/uploads/9182fb60b772873fc40fede7c30b4caa/%E1%84%89%E1%85%B3%E1%84%8F%E1%85%B3%E1%84%85%E1%85%B5%E1%86%AB%E1%84%89%E1%85%A3%E1%86%BA_2023-03-06_%E1%84%8B%E1%85%A9%E1%84%8C%E1%85%A5%E1%86%AB_12.23.01.png" height ="50%" width ="50%"></img>
<img src="https://gitlab.com/flutter_toys/fine-dust-forecasting-app/uploads/0c438d3e642a5f30189b11219cfb5f33/%E1%84%89%E1%85%B3%E1%84%8F%E1%85%B3%E1%84%85%E1%85%B5%E1%86%AB%E1%84%89%E1%85%A3%E1%86%BA_2023-03-06_%E1%84%8B%E1%85%A9%E1%84%8C%E1%85%A5%E1%86%AB_12.23.10.png" height ="50%" width ="50%"></img>
<img src="https://gitlab.com/flutter_toys/fine-dust-forecasting-app/uploads/063943bd1b7419f4cc7c9fe1461d378b/%E1%84%89%E1%85%B3%E1%84%8F%E1%85%B3%E1%84%85%E1%85%B5%E1%86%AB%E1%84%89%E1%85%A3%E1%86%BA_2023-03-06_%E1%84%8B%E1%85%A9%E1%84%8C%E1%85%A5%E1%86%AB_12.23.19.png" height ="50%" width ="50%"></img>
<img src="https://gitlab.com/flutter_toys/fine-dust-forecasting-app/uploads/5c3ba03d5b17f4fbad2eb15dee2a5fdb/%E1%84%89%E1%85%B3%E1%84%8F%E1%85%B3%E1%84%85%E1%85%B5%E1%86%AB%E1%84%89%E1%85%A3%E1%86%BA_2023-03-06_%E1%84%8B%E1%85%A9%E1%84%8C%E1%85%A5%E1%86%AB_12.27.54.png" height ="50%" width ="50%"></img>
</details>

Points of this application

1. **CustomScrollView**

The CustomScrollView allows the **appBar** of the app to import different images depending on the current air pollution **status**, and allows to continue viewing within a certain port range even **when scrolling down**.

2. **Drawer**

If you click the **hamburger button** on the upper left, the drawer pops out, and you can check the status of the area by selecting the desired area.

3. **Supports offline environments**

Configure **Dio**Error to verify your **Internet connection** when importing import data from the API. Even if you can't get the data, showing you the data that was previously stored through **hive**.



